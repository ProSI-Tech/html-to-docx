import logging
import os
import re
import io
from urllib.parse import urlparse, ParseResult
from html.parser import HTMLParser
from enum import Enum
import tempfile

import requests
from docx import Document
from docx.shared import Cm, Mm, Inches, Pt, RGBColor
from docx.enum.text import WD_COLOR_INDEX
from docx.enum.style import WD_STYLE_TYPE
from docx.oxml.shared import OxmlElement, qn

log = logging.getLogger(__name__)


class Color(Enum):
    ALICEBLUE = RGBColor.from_string('F0F8FF')
    ANTIQUEWHITE = RGBColor.from_string('FAEBD7')
    AQUA = RGBColor.from_string('00FFFF')
    AQUAMARINE = RGBColor.from_string('7FFFD4')
    AZURE = RGBColor.from_string('F0FFFF')
    BEIGE = RGBColor.from_string('F5F5DC')
    BISQUE = RGBColor.from_string('FFE4C4')
    BLACK = RGBColor.from_string('000000')
    BLANCHEDALMOND = RGBColor.from_string('FFEBCD')
    BLUE = RGBColor.from_string('0000FF')
    BLUEVIOLET = RGBColor.from_string('8A2BE2')
    BROWN = RGBColor.from_string('A52A2A')
    BURLYWOOD = RGBColor.from_string('DEB887')
    CADETBLUE = RGBColor.from_string('5F9EA0')
    CHARTREUSE = RGBColor.from_string('7FFF00')
    CHOCOLATE = RGBColor.from_string('D2691E')
    CORAL = RGBColor.from_string('FF7F50')
    CORNFLOWERBLUE = RGBColor.from_string('6495ED')
    CORNSILK = RGBColor.from_string('FFF8DC')
    CRIMSON = RGBColor.from_string('DC143C')
    CYAN = RGBColor.from_string('00FFFF')
    DARKBLUE = RGBColor.from_string('00008B')
    DARKCYAN = RGBColor.from_string('008B8B')
    DARKGOLDENROD = RGBColor.from_string('B8860B')
    DARKGRAY = RGBColor.from_string('A9A9A9')
    DARKGREY = RGBColor.from_string('A9A9A9')
    DARKGREEN = RGBColor.from_string('006400')
    DARKKHAKI = RGBColor.from_string('BDB76B')
    DARKMAGENTA = RGBColor.from_string('8B008B')
    DARKOLIVEGREEN = RGBColor.from_string('556B2F')
    DARKORANGE = RGBColor.from_string('FF8C00')
    DARKORCHID = RGBColor.from_string('9932CC')
    DARKRED = RGBColor.from_string('8B0000')
    DARKSALMON = RGBColor.from_string('E9967A')
    DARKSEAGREEN = RGBColor.from_string('8FBC8F')
    DARKSLATEBLUE = RGBColor.from_string('483D8B')
    DARKSLATEGRAY = RGBColor.from_string('2F4F4F')
    DARKSLATEGREY = RGBColor.from_string('2F4F4F')
    DARKTURQUOISE = RGBColor.from_string('00CED1')
    DARKVIOLET = RGBColor.from_string('9400D3')
    DEEPPINK = RGBColor.from_string('FF1493')
    DEEPSKYBLUE = RGBColor.from_string('00BFFF')
    DIMGRAY = RGBColor.from_string('696969')
    DIMGREY = RGBColor.from_string('696969')
    DODGERBLUE = RGBColor.from_string('1E90FF')
    FIREBRICK = RGBColor.from_string('B22222')
    FLORALWHITE = RGBColor.from_string('FFFAF0')
    FORESTGREEN = RGBColor.from_string('228B22')
    FUCHSIA = RGBColor.from_string('FF00FF')
    GAINSBORO = RGBColor.from_string('DCDCDC')
    GHOSTWHITE = RGBColor.from_string('F8F8FF')
    GOLD = RGBColor.from_string('FFD700')
    GOLDENROD = RGBColor.from_string('DAA520')
    GRAY = RGBColor.from_string('808080')
    GREY = RGBColor.from_string('808080')
    GREEN = RGBColor.from_string('008000')
    GREENYELLOW = RGBColor.from_string('ADFF2F')
    HONEYDEW = RGBColor.from_string('F0FFF0')
    HOTPINK = RGBColor.from_string('FF69B4')
    INDIANRED = RGBColor.from_string('CD5C5C')
    INDIGO = RGBColor.from_string('4B0082')
    IVORY = RGBColor.from_string('FFFFF0')
    KHAKI = RGBColor.from_string('F0E68C')
    LAVENDER = RGBColor.from_string('E6E6FA')
    LAVENDERBLUSH = RGBColor.from_string('FFF0F5')
    LAWNGREEN = RGBColor.from_string('7CFC00')
    LEMONCHIFFON = RGBColor.from_string('FFFACD')
    LIGHTBLUE = RGBColor.from_string('ADD8E6')
    LIGHTCORAL = RGBColor.from_string('F08080')
    LIGHTCYAN = RGBColor.from_string('E0FFFF')
    LIGHTGOLDENRODYELLOW = RGBColor.from_string('FAFAD2')
    LIGHTGRAY = RGBColor.from_string('D3D3D3')
    LIGHTGREY = RGBColor.from_string('D3D3D3')
    LIGHTGREEN = RGBColor.from_string('90EE90')
    LIGHTPINK = RGBColor.from_string('FFB6C1')
    LIGHTSALMON = RGBColor.from_string('FFA07A')
    LIGHTSEAGREEN = RGBColor.from_string('20B2AA')
    LIGHTSKYBLUE = RGBColor.from_string('87CEFA')
    LIGHTSLATEGRAY = RGBColor.from_string('778899')
    LIGHTSLATEGREY = RGBColor.from_string('778899')
    LIGHTSTEELBLUE = RGBColor.from_string('B0C4DE')
    LIGHTYELLOW = RGBColor.from_string('FFFFE0')
    LIME = RGBColor.from_string('00FF00')
    LIMEGREEN = RGBColor.from_string('32CD32')
    LINEN = RGBColor.from_string('FAF0E6')
    MAGENTA = RGBColor.from_string('FF00FF')
    MAROON = RGBColor.from_string('800000')
    MEDIUMAQUAMARINE = RGBColor.from_string('66CDAA')
    MEDIUMBLUE = RGBColor.from_string('0000CD')
    MEDIUMORCHID = RGBColor.from_string('BA55D3')
    MEDIUMPURPLE = RGBColor.from_string('9370DB')
    MEDIUMSEAGREEN = RGBColor.from_string('3CB371')
    MEDIUMSLATEBLUE = RGBColor.from_string('7B68EE')
    MEDIUMSPRINGGREEN = RGBColor.from_string('00FA9A')
    MEDIUMTURQUOISE = RGBColor.from_string('48D1CC')
    MEDIUMVIOLETRED = RGBColor.from_string('C71585')
    MIDNIGHTBLUE = RGBColor.from_string('191970')
    MINTCREAM = RGBColor.from_string('F5FFFA')
    MISTYROSE = RGBColor.from_string('FFE4E1')
    MOCCASIN = RGBColor.from_string('FFE4B5')
    NAVAJOWHITE = RGBColor.from_string('FFDEAD')
    NAVY = RGBColor.from_string('000080')
    OLDLACE = RGBColor.from_string('FDF5E6')
    OLIVE = RGBColor.from_string('808000')
    OLIVEDRAB = RGBColor.from_string('6B8E23')
    ORANGE = RGBColor.from_string('FFA500')
    ORANGERED = RGBColor.from_string('FF4500')
    ORCHID = RGBColor.from_string('DA70D6')
    PALEGOLDENROD = RGBColor.from_string('EEE8AA')
    PALEGREEN = RGBColor.from_string('98FB98')
    PALETURQUOISE = RGBColor.from_string('AFEEEE')
    PALEVIOLETRED = RGBColor.from_string('DB7093')
    PAPAYAWHIP = RGBColor.from_string('FFEFD5')
    PEACHPUFF = RGBColor.from_string('FFDAB9')
    PERU = RGBColor.from_string('CD853F')
    PINK = RGBColor.from_string('FFC0CB')
    PLUM = RGBColor.from_string('DDA0DD')
    POWDERBLUE = RGBColor.from_string('B0E0E6')
    PURPLE = RGBColor.from_string('800080')
    REBECCAPURPLE = RGBColor.from_string('663399')
    RED = RGBColor.from_string('FF0000')
    ROSYBROWN = RGBColor.from_string('BC8F8F')
    ROYALBLUE = RGBColor.from_string('4169E1')
    SADDLEBROWN = RGBColor.from_string('8B4513')
    SALMON = RGBColor.from_string('FA8072')
    SANDYBROWN = RGBColor.from_string('F4A460')
    SEAGREEN = RGBColor.from_string('2E8B57')
    SEASHELL = RGBColor.from_string('FFF5EE')
    SIENNA = RGBColor.from_string('A0522D')
    SILVER = RGBColor.from_string('C0C0C0')
    SKYBLUE = RGBColor.from_string('87CEEB')
    SLATEBLUE = RGBColor.from_string('6A5ACD')
    SLATEGRAY = RGBColor.from_string('708090')
    SLATEGREY = RGBColor.from_string('708090')
    SNOW = RGBColor.from_string('FFFAFA')
    SPRINGGREEN = RGBColor.from_string('00FF7F')
    STEELBLUE = RGBColor.from_string('4682B4')
    TAN = RGBColor.from_string('D2B48C')
    TEAL = RGBColor.from_string('008080')
    THISTLE = RGBColor.from_string('D8BFD8')
    TOMATO = RGBColor.from_string('FF6347')
    TURQUOISE = RGBColor.from_string('40E0D0')
    VIOLET = RGBColor.from_string('EE82EE')
    WHEAT = RGBColor.from_string('F5DEB3')
    WHITE = RGBColor.from_string('FFFFFF')
    WHITESMOKE = RGBColor.from_string('F5F5F5')
    YELLOW = RGBColor.from_string('FFFF00')
    YELLOWGREEN = RGBColor.from_string('9ACD32')


class DocGeneric:
    def __init__(self, parent, document, attrs):
        self.parent = parent
        self.doc = document
        self.attrs = dict(attrs)
        if 'style' in self.attrs:
            self.styles = dict(
                re.split(r'\s*:\s*', x) for x in re.split(r'\s*;\s*', self.attrs['style']) if x)
        else:
            self.styles = {}
        self.first = True
        self.hclass = self.attrs.get('class')
        self.p = None
        self.r = None
        if parent:
            self.style = parent.style
            self.level = parent.level
            self.cell = self.parent.cell
            self.cell_first_p = self.parent.cell_first_p
            if not self.hclass:
                self.hclass = self.parent.hclass
            elif not isinstance(self, Table):
                self.style = self.hclass
        else:
            self.style = None
            self.level = -1
            self.cell = None
            self.cell_first_p = False


class DocContainer(DocGeneric):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)

    def add_paragraph(self, style=None, parent=None, p=None):
        if not style:
            style = self.style

        if parent:
            p = parent.add_paragraph()
        elif not p:
            p = self.doc.add_paragraph()

        # Apply style
        try:
            p.style = style
        except Exception as e:
            log.debug(e)

        return p

    def add_run(self, data=None):
        if self.p and self.parent:
            r = self.p.add_run(data)
        else:
            self.p = self.add_paragraph(style=self.style)
            r = self.p.add_run(data)

        return r

    def add_picture(self, image, width=None, height=None):
        # image parameter not already converted
        log.debug('\tImage INSTANCE is "{0}"'.format(type(image)))
        if not (isinstance(image, io.BytesIO) or isinstance(image, ParseResult)):
            url_parts = urlparse(image)
            # Web URL
            if all([url_parts.scheme, url_parts.netloc, url_parts.path]):
                session = HTML2DOCX.SESSION or requests.Session()
                try:
                    http_resp = session.get(image)
                    if http_resp.ok:
                        image = io.BytesIO(http_resp.content)
                    else:
                        http_resp.raise_for_status()

                    content_type = http_resp.headers['Content-Type']
                    if not re.search(r'^image', content_type):
                        log.warning('Found "{0}" instead of image content!'.format(content_type))
                        return
                except Exception as e:
                    if log.getEffectiveLevel() <= logging.DEBUG:
                        log.exception(e)
                    else:
                        log.error(e)
                    return
            # Probably a file path URL
            elif url_parts.scheme == 'file':
                image = url_parts.path

        log.debug('\t\tWIDTH: {0} - HEIGHT: {1}'.format(width, height))
        shape = self.add_run().add_picture(image, width, height)
        if shape.width > self.doc._block_width:
            factor = self.doc._block_width / shape.width
            shape.width = int(shape.width * factor)
            shape.height = int(shape.height * factor)

    def add_table(self):
        if self.cell:
            return self.cell.add_table(0, 0)
        else:
            return self.doc.add_table(0, 0)

    def fill(self, data):
        if data:
            self.r = self.add_run(data)


class DocFormat(DocGeneric):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = None
        self.value = None

        # Do not create a new run if parent is a DocFormat
        if isinstance(self.parent, DocFormat):
            self.r = self.parent.r
        else:
            self.r = self.parent.add_run()

    def add_paragraph(self, style=None):
        if not self.parent.p:
            self.parent.p = self.parent.add_paragraph()

        p = self.parent.p
        if style:
            try:
                p.style = style
            except Exception as e:
                log.debug(e)

        return p

    def fill(self, data):
        if data:
            self.r.add_text(data)

    def __del__(self):
        if self.r and self.name and self.value:
            if self.name == 'color':
                self.r.font.color.rgb = self.value
            else:
                self.r.font.__setattr__(self.name, self.value)


class Paragraph(DocContainer):
    def add_paragraph(self, style=None, parent=None, p=None):
        if parent:
            p = super().add_paragraph(style, parent=parent)
        else:
            p = super().add_paragraph(style, parent=self.parent)

        return p


class Header1(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=1)


class Header2(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=2)


class Header3(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=3)


class Header4(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=4)


class Header5(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=5)


class Header6(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=6)


class Header7(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=7)


class Header8(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=8)


class Header9(Paragraph):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.p = self.doc.add_heading(level=9)


class UnorderedList(DocGeneric):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.level += 1
        if self.level:
            self.style = 'List Bullet {0}'.format(self.level + 1)
        else:
            self.style = 'List Bullet'

        log.debug('\tList Level: {0}'.format(self.level))

    def __del__(self):
        self.level -= 1


class OrderedList(UnorderedList):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        if self.level:
            self.style = 'List Number {0}'.format(self.level + 1)
        else:
            self.style = 'List Number'

    def reset_numbering(self, par, prev=None, level=None, num=True):
        """
        This code is taken as is from the answer of Joseph Fox-Rabinoviz
        [https://github.com/madphysicist]to python-openxl github issue 25
        [https://github.com/python-openxml/python-docx/issues/25].
        Just adapted it to be used as a method.

        Makes a paragraph into a list item with a specific level and
        optional restart.

        An attempt will be made to retreive an abstract numbering style that
        corresponds to the style of the paragraph. If that is not possible,
        the default numbering or bullet style will be used based on the
        ``num`` parameter.

        Parameters
        ----------
        par : docx.paragraph.Paragraph
            The paragraph to turn into a list item.
        prev : docx.paragraph.Paragraph or None
            The previous paragraph in the list. If specified, the numbering
            and styles will be taken as a continuation of this paragraph.
            If omitted, a new numbering scheme will be started.
        level : int or None
            The level of the paragraph within the outline. If ``prev`` is
            set, defaults to the same level as in ``prev``. Otherwise,
            defaults to zero.
        num : bool
            If ``prev`` is :py:obj:`None` and the style of the paragraph
            does not correspond to an existing numbering style, this will
            determine wether or not the list will be numbered or bulleted.
            The result is not guaranteed, but is fairly safe for most Word
            templates.
        """
        xpath_options = {
            True: {'single': 'count(w:lvl)=1 and ', 'level': 0},
            False: {'single': '', 'level': level},
        }

        def style_xpath(prefer_single=True):
            """
            The style comes from the outer-scope variable ``par.style.name``.
            """
            style = par.style.style_id
            return (
                'w:abstractNum['
                    '{single}w:lvl[@w:ilvl="{level}"]/w:pStyle[@w:val="{style}"]'
                ']/@w:abstractNumId'
            ).format(style=style, **xpath_options[prefer_single])

        def type_xpath(prefer_single=True):
            """
            The type is from the outer-scope variable ``num``.
            """
            type = 'decimal' if num else 'bullet'
            return (
                'w:abstractNum['
                    '{single}w:lvl[@w:ilvl="{level}"]/w:numFmt[@w:val="{type}"]'
                ']/@w:abstractNumId'
            ).format(type=type, **xpath_options[prefer_single])

        def get_abstract_id():
            """
            Select as follows:

                1. Match single-level by style (get min ID)
                2. Match exact style and level (get min ID)
                3. Match single-level decimal/bullet types (get min ID)
                4. Match decimal/bullet in requested level (get min ID)
                3. 0
            """
            for fn in (style_xpath, type_xpath):
                for prefer_single in (True, False):
                    xpath = fn(prefer_single)
                    ids = numbering.xpath(xpath)
                    if ids:
                        return min(int(x) for x in ids)

            return 0

        if (prev is None or
                prev._p.pPr is None or
                prev._p.pPr.numPr is None or
                prev._p.pPr.numPr.numId is None):
            if level is None:
                level = 0
            numbering = self.doc.part.numbering_part.numbering_definitions._numbering
            # Compute the abstract ID first by style, then by num
            anum = get_abstract_id()
            # Set the concrete numbering based on the abstract numbering ID
            num = numbering.add_num(anum)
            # Make sure to override the abstract continuation property
            num.add_lvlOverride(ilvl=level).add_startOverride(1)
            # Extract the newly-allocated concrete numbering ID
            num = num.numId
        else:
            if level is None:
                level = prev._p.pPr.numPr.ilvl.val
            # Get the previous concrete numbering ID
            num = prev._p.pPr.numPr.numId.val
        par._p.get_or_add_pPr().get_or_add_numPr().get_or_add_numId().val = num
        par._p.get_or_add_pPr().get_or_add_numPr().get_or_add_ilvl().val = level

class ListItem(Paragraph):
    def add_paragraph(self, style=None):
        p = self.parent.parent.add_paragraph(style)

        # Reset numbering if needed. Always to be put after setting style (in super())
        if isinstance(self.parent, OrderedList) and self.parent.first:
            self.parent.first = False
            self.parent.reset_numbering(p)

        return p


class BlockQuote(DocContainer):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.style = 'Intense Quote'


class HorizontalLine(DocContainer):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.add_picture(os.path.join(os.path.dirname(__file__), 'Line.png'))


class Image(DocContainer):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        img_path = self.attrs.get('src')
        width = self.attrs.get('width')
        if width:
            width = Pt(int(width))

        height = self.attrs.get('height')
        if height:
            height = Pt(int(height))

        if img_path:
            self.add_picture(img_path, width=width, height=height)
        else:
            log.error('***ERROR*** No image path is provided!')


class Anchor(DocFormat):
    def fill(self, data):
        """
        This code is taken as is from the answer of Ryan Rushton
        [https://github.com/rushton3179] to python-openxml github issue 74
        [https://github.com/python-openxml/python-docx/issues/74].

        A function that places a hyperlink within a paragraph object.
        """

        url = self.attrs.get('href')

        from docx.opc.constants import RELATIONSHIP_TYPE as RT
        from docx.enum.dml import MSO_THEME_COLOR_INDEX

        # This gets access to the document.xml.rels file and gets a new relation id value
        part = self.r.part
        r_id = part.relate_to(url, RT.HYPERLINK, is_external=True)

        # Create the w:hyperlink tag and add needed values
        hyperlink = OxmlElement('w:hyperlink')
        hyperlink.set(qn('r:id'), r_id, )
        hyperlink.set(qn('w:history'), '1')

        # Create a w:r element
        new_run = OxmlElement('w:r')

        # Create a new w:rPr element
        rPr = OxmlElement('w:rPr')

        # Create a w:rStyle element, note this currently does not add the hyperlink
        # style as its not in the default template, I have left it here in case
        # someone uses one that has the style in it
        rStyle = OxmlElement('w:rStyle')
        rStyle.set(qn('w:val'), 'Hyperlink')

        # Join all the xml elements together add add the required data to the w:r element
        rPr.append(rStyle)
        new_run.append(rPr)
        new_run.text = data
        hyperlink.append(new_run)

        # Create a new Run object and add the hyperlink into it
        #self.r = self.p.add_run()
        self.r._r.append(hyperlink)

        # A workaround for the lack of a hyperlink style (doesn't go purple after using the link)
        # Delete this if using a template that has the hyperlink style in it
        self.r.font.color.theme_color = MSO_THEME_COLOR_INDEX.HYPERLINK
        self.r.font.underline = True


class Break(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.r.add_break()


class Bold(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = 'bold'
        self.value = True


class Italic(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = 'italic'
        self.value = True


class StrikeThrough(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = 'strike'
        self.value = True


class Underline(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = 'underline'
        self.value = True


class Superscript(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = 'superscript'
        self.value = True


class Subscript(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = 'subscript'
        self.value = True


class Citation(DocFormat):
    def fill(self, data):
        data = '({0})'.format(data)
        super().fill(data)


class Monospaced(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.name = 'name'
        self.value = 'Courier New'


class Font(DocFormat):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        for attr in self.attrs:
            if attr == 'color':
                self.name = 'color'
                if re.search(r'^#[0-9a-fA-F]+$', self.attrs[attr]):
                    self.value = RGBColor.from_string(self.attrs[attr][1:])
                elif hasattr(Color, self.attrs[attr].upper()):
                    self.value = Color[self.attrs[attr].upper()].value
                else:
                    log.warning('Unsupported color definition "{0}"!'.format(self.attrs[attr]))
            else:
                log.warning('Unsupported font attribute "{0}"!'.format(attr))


class _Table(DocGeneric):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.row_index = -1
        self.column_index = -1
        self.rows_in_table_header = False
        self.rows_in_table_footer = True
        self.rowspans = []

    def add_row(self):
        self.row_index += 1
        self.column_index = -1
        row = self.table.add_row()
        log.debug('\tAdded row {0}'.format(self.row_index))
        return row

    def merge_columns(self, colspan, rowspan, is_header):
        if colspan:
            log.debug('\t\t\t*****Merging with columns {0}'.format(self.column_index + 1))
            # Add a cell and merge with the current one
            self.table.cell(self.row_index, self.column_index).merge(
                self.add_cell(colspan=colspan - 1, rowspan=rowspan, is_header=is_header))

    def merge_rows(self, rowspan, is_header):
        if self.rowspans[self.column_index]:
            log.debug(
                '\t\t*****Merging column {0} with previous row column'.format(self.column_index))
            # Merge current cell with the one in the previous row same column
            self.table.cell(self.row_index - 1, self.column_index).merge(
                self.table.cell(self.row_index, self.column_index))
            self.rowspans[self.column_index] -= 1
            # Add a new cell to ignore this one. No HTML <td> is expected for it
            self.add_cell(colspan=0, rowspan=rowspan, is_header=is_header)
        elif rowspan:
            self.rowspans[self.column_index] = rowspan
        else:
            self.rowspans[self.column_index] = 0

        return

    def set_current_cell_as_header(self):
        '''w:cnfStyle w:val="100000000000" w:lastColumn="0" w:firstColumn="0" w:lastRow="0"
           w:firstRow="1" w:lastRowLastColumn="0" w:lastRowFirstColumn="0"
           w:firstRowLastColumn="0" w:firstRowFirstColumn="0" w:evenHBand="0" w:oddHBand="0"
           w:evenVBand="0" w:oddVBand="0"'''
        # Update the row style
        tr = self.table.rows[self.row_index]._element
        tr_pr = None
        cnf_styles = tr.xpath('./w:trPr/w:cnfStyle')
        if cnf_styles:
            tr_cnf_style = cnf_styles[0]
        else:
            prs = tr.xpath('./w:trPr')
            if prs:
                tr_pr = prs[0]
            else:
                tr_pr = OxmlElement('w:trPr')
                tr.insert(0, tr_pr)

            # Add the cnfStyle element
            tr_cnf_style = OxmlElement('w:cnfStyle')
            tr_pr.append(tr_cnf_style)

        # Set the firstRow attribute
        tr_cnf_style.set(qn('w:firstRow'), "1")

        # Update the cell style
        tc = self.table.cell(self.row_index, self.column_index)._element
        tc_pr = None
        cnf_styles = tc.xpath('./w:tcPr/w:cnfStyle')
        if cnf_styles:
            tc_cnf_style = cnf_styles[0]
        else:
            prs = tr.xpath('./w:tcPr')
            if prs:
                tc_pr = prs[0]
            else:
                tc_pr = OxmlElement('w:tcPr')
                tc.insert(0, tc_pr)

            # Add the cnfStyle element
            tc_cnf_style = OxmlElement('w:cnfStyle')
            tc_pr.append(tc_cnf_style)

        # Set the firstRow attribute and the firstColumn if it is the first cell in a row
        tc_cnf_style.set(qn('w:firstRow'), "1")
        if not self.column_index:
            tc_cnf_style.set(qn('w:firstColumn'), "1")

        return

    def add_cell(self, colspan=0, rowspan=0, width=0, is_header=False):
        self.column_index += 1
        log.debug(
            '\t\tAdding cell: row {0}, column {1} '.format(self.row_index, self.column_index) \
                + 'having a colspan/rowspan of {0}/{1} and a width of {2}'.format(
                    colspan, rowspan, width))

        # Check if the column exists otherwise add it
        # (in case rows do not have same columns count)
        # Otherwise set column width if needed.
        if self.column_index >= len(self.table.rows[self.row_index].cells):
            # If width is not provided the table becomes auto fit and uses max sizes
            if width is None:
                width = self.doc._block_width
                self.table.autofit = True

            self.table.add_column(width)
        elif self.table.columns[self.column_index].width:
            # Force width to previous column width if already set
            width = self.table.columns[self.column_index].width

        self.table.columns[self.column_index].width = width

        # Updating self.rowspans array in case of a new column
        if self.column_index >= len(self.rowspans):
            self.rowspans.append(0)

        # Make the current cell as a header cell if needed
        if is_header:
            self.set_current_cell_as_header()

        # Merge columns
        self.merge_columns(colspan, rowspan, is_header)

        # Merge rows in selected columns and update self.rowspans list info
        self.merge_rows(rowspan, is_header)

        # Return the current cell:
        return self.table.cell(self.row_index, self.column_index)


class Table(_Table):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        log.debug('Create Table')
        self.table = self.parent.add_table()
        self.table.autofit = False
        if self.hclass:
            try:
                self.table.style = self.hclass
            except KeyError as e:
                log.debug('{0} Defaulting to "Table Grid"'.format(e))
                self.table.style = 'Table Grid'
        else:
            self.table.style = 'Table Grid'


class _Table_Component(_Table):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.table =self.parent.table
        self.row_index = self.parent.row_index
        self.column_index = self.parent.column_index

    def __del__(self):
        self.parent.row_index = self.row_index
        self.parent.column_index = self.column_index


class TableHeader(_Table_Component):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.rows_in_table_header = True

    def add_row(self):
        tr = super().add_row()._element
        log.debug('  Make the row as a header to repeat')
        tr_pr = None
        cnf_tbl_header = tr.xpath('./w:trPr/w:tblHeader')
        if not cnf_tbl_header:
            tr_pr = tr.xpath('./w:trPr')
            if not tr_pr:
                tr_pr = OxmlElement('w:trPr')
                tr.insert(0, tr_pr)

            # Add the tblHeader element
            cnf_style = OxmlElement('w:tblHeader')
            tr_pr.append(cnf_style)


class TableBody(_Table_Component):
    pass


class TableRow(DocGeneric):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.row = self.parent.add_row()

    def add_cell(self, colspan=0, rowspan=0, width=0, is_header=False):
        return self.parent.add_cell(colspan, rowspan, width, is_header)


class TableCell(DocContainer):
    def __init__(self, parent, document, attrs, is_header=False):
        super().__init__(parent, document, attrs)

        colspan = int(self.attrs.get('colspan', 0))
        if colspan:
            colspan -= 1

        rowspan = int(self.attrs.get('rowspan', 0))
        if rowspan:
            rowspan -= 1

        width = self.styles.get('width')
        if width:
            if width[-2:].lower() == 'cm':
                width = Cm(float(width[:-2]))
            elif width[-2:].lower() == 'mm':
                width = Mm(float(width[:-2]))
            elif width[-2:].lower() == 'in':
                width = Inches(float(width[:-2]))
            elif width[-2:].lower() == 'pt':
                width = Pt(float(width[:-2]))
            elif width[-1:].lower() == '%':
                width = int(self.doc._block_width * float(width[:-1]) / 100)
            else:
                log.error('*****ERROR***** Unknown width format "{0}"!'.format(width))

        self.cell = self.parent.add_cell(colspan, rowspan, width, is_header)
        self.cell_first_p = True

    def add_paragraph(self, style=None):
        if self.cell_first_p:
            self.cell_first_p = False
            return super().add_paragraph(style=style, p=self.cell.paragraphs[-1])
        else:
            return super().add_paragraph(style=style, p=self.cell.add_paragraph())


class TableHeaderCell(TableCell):
    def __init__(self, parent, document, attrs, is_header=True):
        super().__init__(parent, document, attrs, is_header)


class TableFooter(_Table_Component):
    def __init__(self, parent, document, attrs):
        super().__init__(parent, document, attrs)
        self.rows_in_table_footer = True


class HTML2DOCX(HTMLParser):
    IGNORE_TAGS = ('html', 'head', 'body')
    BYPASS_TAGS = ('meta', 'link', 'title', 'style')
    TAG_TO_CLASS = {
            'p': Paragraph,
            'h1': Header1,
            'h2': Header2,
            'h3': Header3,
            'h4': Header4,
            'h5': Header5,
            'h6': Header6,
            'h7': Header7,
            'h8': Header8,
            'h9': Header9,
            'a': Anchor,
            'div': Paragraph,
            'blockquote': BlockQuote,
            'span': DocFormat,
            'pre': DocFormat,
            'hr': HorizontalLine,
            'br': Break,
            'img': Image,
            'b': Bold,
            'em': Italic,
            'cite': Citation,
            'del': StrikeThrough,
            'ins': Underline,
            'sup': Superscript,
            'sub': Subscript,
            'tt': Monospaced,
            'code': Monospaced,
            'font': Font,
            'ul': UnorderedList,
            'ol': OrderedList,
            'li': ListItem,
            'table': Table,
            'tbody': TableBody,
            'thead': TableHeader,
            'tfoot': TableFooter,
            'tr': TableRow,
            'th': TableHeaderCell,
            'td': TableCell,
            'container': DocContainer,
            'unsupported': DocFormat
        }
    SESSION = None

    def __init__(self, template=None, title='', add_section=False, html_session=None):
        super().__init__()
        # Open the template docx document
        self.doc = Document(template)

        if add_section:
            self.doc.add_section()

        # Check if the last paragraph is empty, then delete it
        elif self.doc.paragraphs:
            paragraph = self.doc.paragraphs[-1]
            if not all((paragraph.runs, paragraph.text)):
                p_element = paragraph._element
                p_element.getparent().remove(p_element)

        # Set document properties
        self.set_property('title', title)

        HTML2DOCX.SESSION = html_session
        self.tag_obj_list = [DocContainer(None, self.doc, [])]
        self.containers = []
        self.previous_tag = None
        self.ignore_tags_count = 0
        self.bypass = False

        if log.getEffectiveLevel() <= logging.DEBUG:
            self.html_fd = tempfile.TemporaryFile(mode='w+', encoding='utf-8')

    def set_property(self, prop_name, prop_value):
        setattr(self.doc.core_properties, prop_name, prop_value)

    def add_html(self, html_str, ignore_tags_count=0):
        self.ignore_tags_count = ignore_tags_count
        html_str = re.sub(r'\s*(\n|\r)+\s*(\n|\r)*<', r'<', html_str)
        self.feed(html_str)
        if log.getEffectiveLevel() <= logging.DEBUG:
            self.html_fd.write(html_str)

    def save(self, document_path):
        if log.getEffectiveLevel() <= logging.DEBUG:
            log.debug('Saving XML and HTML versions of {0}'.format(document_path))
            fd = open('{0}.xml'.format(document_path), mode='w', encoding='utf-8')
            fd.write(self.doc.element.xml)
            fd.close()
            fd = open('{0}.html'.format(document_path), mode='w', encoding='utf-8')
            self.html_fd.seek(0)
            fd.write(self.html_fd.read())
            fd.close()
            self.html_fd.close()

        self.doc.save(document_path)

    def handle_starttag(self, tag, attrs):
        if tag in self.IGNORE_TAGS:
            # Ignore some pre-defined tags
            log.debug('Ignored start tag: "{0}" with attrs: {1}'.format(tag, attrs))
            return
        elif tag in self.BYPASS_TAGS:
            # Bypass some other pre-defined tags
            self.bypass = True
            log.debug('Bypassed start tag: "{0}" with attrs: {1}'.format(tag, attrs))
            return

        log.debug('Encountered start tag: "{0}" with attrs: {1}'.format(tag, attrs))
        parent_tag_obj = self.tag_obj_list[-1]

        # Ignore first tags if needed
        if self.ignore_tags_count:
            self.ignore_tags_count -= 1
            tag = 'unsupported'
        elif tag in self.TAG_TO_CLASS:
            # If previous tag was a table insert an empty paragraph
            if tag == 'table' and self.previous_tag == 'table':
                log.debug('Previous tag: "{0}"'.format(self.previous_tag))
                paragraph = Paragraph(parent_tag_obj, self.doc, attrs)
                paragraph.style = 'Normal'
                paragraph.add_run()
        else:
            log.warning('Unsupported tag "{0}"!'.format(tag))
            tag = 'unsupported'

        tag_obj = self.TAG_TO_CLASS[tag](parent_tag_obj, self.doc, attrs)
        self.tag_obj_list.append(tag_obj)

    def handle_endtag(self, tag):
        #Ignore some tags
        if tag in self.IGNORE_TAGS:
            log.debug('Ignored end tag: "{0}"'.format(tag))
            return
        if tag in self.BYPASS_TAGS:
            self.bypass = False
            log.debug('Bypassed end tag: "{0}"'.format(tag))
            return

        log.debug('Encountered end tag : "{0}"'.format(tag))

        self.previous_tag = tag
        del self.tag_obj_list[-1]

    def handle_data(self, data):
        if self.bypass:
            log.debug('Bypassed encountered data: "{0}"'.format(data))
            return

        log.debug('Encountered data: "{0}"'.format(data))
        log.debug('Stack  : "{0}"'.format(self.tag_obj_list))
        if self.tag_obj_list:
            self.tag_obj_list[-1].fill(re.sub(r'^(\n|\r)+\s*', '', data))

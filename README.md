# README #

This README documents steps necessary to install ***html-to-docx*** library
on your system.

## What is this repository for? ##

* ***html-to-docx*** is a quick and dirty pure Python module developed in order
to convert html to Microsoft .docx documents.

* Version: 0.0.3 (Alpha)

* [Home](https://bitbucket.org/ProSI-Tech/html-to-docx)

## How do I get set up? ##

**If you are behind a proxy** set the following environment variables:

* **`http_proxy`** to **`http://<your_proxy_hostname_or_ip>:<port_number>`**

* **`https_proxy`** to
**`https://<your_proxy_hostname_or_ip>:<port_number>`**

### Using pip: ###
* One step install using pip:  
**`pip install git+https://bitbucket.org/ProSI-Tech/html-to-docx.git`**

### Using setup.py: ###
* Clone the repository using:  
**`git clone https://bitbucket.org/ProSI-Tech/html-to-docx.git`**

* Then change directory:  
**`cd html-to-docx`**

* Then run:  
**`python setup install`**

This will install modules in the default system install folders which might
require root access. You could, instead set your environment variable
**`PYTHONPATH`** to local paths as for example:  
**`${HOME}/.local/lib/python3.7:${HOME}/.local/lib/python3.7/site-packages`**

then execute:  
**`python setup.py install --prefix=${HOME}/.local/lib`**.  

### Dependencies: ###
This should automatically install the following dependencies:

* [python-docx](https://pypi.org/project/python-docx);

* [requests](https://pypi.org/project/requests).

## Contribution guidelines ##

### Development rules: ###

* Syntax **shall** support Python 3.6 and higher

* Code **shall** be operating system agnostic, and **shall**, at least
support Microsoft OSs, and any Unix based OS.

* Syntax *should* preferably support Python 2.7+, and Python 3+

### Writing tests ###

### Code review ###

## Who do I talk to? ##

Samir Khellaf [opensource@prosi-tech.com](mailto:opensource@prosi-tech.com)
